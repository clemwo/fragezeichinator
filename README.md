# Fragezeichinator

![](https://i.imgur.com/0zalpgM.png)

## About

[Fragezeichinator.de](https://Fragezeichinator.de) is a simple static website that spits out Spotify links to a random episode of the famous german language radio play "Die Drei Fragezeichen ???"

Besides filtering by release year the episodes can also be filtered by rating; the scale reaching from 1 (very good) to 6 (very bad). The episode data such as release year is requested directly from Spotify API while the user rating is scraped from the fan page [rocky-beach.com](https://www.rocky-beach.com) 

The script for getting the data can be found [here](https://gitlab.com/clemwo/fragescraper)

## Technology

This page is hosted as a simple GitLab Page which was fairly easy to set up. As a CSS framework Bootstrap was used. Other than that no crazy technologies involved, only jquery and a bunch of JavaScript.

Please note that this is one of my first web development project so this is very ugly. In the mean time I've been learning React (MERN-Stack to be precise) and plan on rebuilding and improving the project.

I used this as a website template:
[Start Bootstrap - Scrolling Nav](https://startbootstrap.com/template/scrolling-nav/)