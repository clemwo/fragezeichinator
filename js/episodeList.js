class EpisodeList {
    _allEpisodes;
    _ratingFrom;
    _ratingTo;
    _yearFrom;
    _yearTo;
    _episodePath;
    _allEpisodes;

    constructor(episodePath) {
        this._ratingFrom = 1.0
        this._ratingTo = 6.0
        this._yearFrom = 1979
        this._yearTo = 2022
        this._episodePath = episodePath;
        $.getJSON("ddf_albums.json", function (data) {
                this._allEpisodes = data
            }
        ).fail(function () {
            console.log("An error reading the json has occurred.");
        })
    }

    get episodePath() {
        return this._episodePath;
    }

    set episodePath(value) {
        this._episodePath = value;
    }

    get ratingFrom() {
        return this._ratingFrom;
    }

    set ratingFrom(value) {
        this._ratingFrom = value;
    }

    get ratingTo() {
        return this._ratingTo;
    }

    set ratingTo(value) {
        this._ratingTo = value;
    }

    get yearFrom() {
        return this._yearFrom;
    }

    set yearFrom(value) {
        this._yearFrom = value;
    }

    get yearTo() {
        return this._yearTo;
    }

    set yearTo(value) {
        this._yearTo = value;
    }
}