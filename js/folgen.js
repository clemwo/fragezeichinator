let ratingFrom = 1.0
let ratingTo = 6.0
let yearFrom = 1979
let yearTo = new Date().getFullYear()


let filteredEpisodes
let randomEpisode

$(document).ready(function () {
    $.getJSON("ddf_albums.json", function (data) {
            episodeList = data
        }
    ).fail(function () {
        console.log("An error reading the json has occurred.");
    })
})

function printEpisode() {
    $(document).ready(function () {
        const rating_from = parseFloat(document.getElementById("ratingFromOutput").value)
        const rating_to = parseFloat(document.getElementById("ratingToOutput").value)

        // TODO: Firefox has an issue with the date parsing when opening the page, this is just a quick dirty fix
        let year_from = Date.parse(document.getElementById("yearFromOutput").value)
        if (isNaN(year_from)) {
            year_from = Date.parse("1979");
        }
        let year_to = Date.parse(document.getElementById("yearToOutput").value);
        if (isNaN(year_to)) {
            year_to = Date.parse("2022")
        }
        let specialsCheck = document.getElementById("specials").checked
        filterEpisodes(rating_from, rating_to, year_from, year_to, specialsCheck)
        // now pick a random episode from the list of filtered episodes
        // TODO: Error when filteredEpisodes is empty
        randomEpisode = filteredEpisodes[Math.floor(Math.random() * filteredEpisodes.length)];
        const randomEpisodeCoverURL = randomEpisode.images[1].url
        const randomEpisodeLink = randomEpisode["external_urls"]["spotify"]
        const resultHtml = "<a href='" + randomEpisodeLink + "'" + "target='_blank'><img src='" + randomEpisodeCoverURL + "' className='img-fluid' alt='Responsive image'></a>"
        document.getElementById("resultImg").innerHTML = resultHtml
        showHideRating()
        const releaseDate = new Date(Date.parse(randomEpisode["release_date"])).getFullYear()
        document.getElementById("resultYear").innerHTML = "Releasejahr: " + releaseDate
    })
}

function showHideRating() {
    if (document.getElementById("showRating").checked) {
        document.getElementById("resultRating").innerHTML = "<a href='https://www.rocky-beach.com/php/project/f_ausgabe.html' target=\"_blank\" rel=\"noopener noreferrer\">Bewertung</a>: " +
            randomEpisode["rating"]
    } else {
        document.getElementById("resultRating").innerHTML = "<a href='https://www.rocky-beach.com/php/project/f_ausgabe.html' target=\"_blank\" rel=\"noopener noreferrer\">Bewertung</a>: versteckt"
    }
}

function filterEpisodes(ratingFrom, ratingTo, yearFrom, yearTo, specialsCheck) {
    filteredEpisodes = episodeList.filter(episode => episode["rating"] > ratingFrom && episode["rating"] < ratingTo && Date.parse(episode["release_date"]) > yearFrom && Date.parse(episode["release_date"]) < yearTo)
    if (specialsCheck) {
        let specialEpisodes = episodeList.filter(episode => episode["special_version"] == true && Date.parse(episode["release_date"]) > yearFrom && Date.parse(episode["release_date"]) < yearTo)
        filteredEpisodes.push(...specialEpisodes)
    }
    console.log(filteredEpisodes)
}

function setRatingFrom(rating) {
    if (rating > ratingTo) {
        console.log("Error, rating from can't be higher than rating to.")
        document.getElementById("ratingFromInput").value = ratingFrom
        return false
    }
    ratingFrom = rating
    document.getElementById("ratingFromOutput").value = ratingFrom
    return true
}

function setRatingTo(rating) {
    if (rating < ratingFrom) {
        console.log("Error, rating to can't be lower than rating from.")
        document.getElementById("ratingToInput").value = ratingTo
        return false
    }
    ratingTo = rating
    document.getElementById("ratingToOutput").value = ratingTo
    return true
}

function setYearFrom(year) {
    if (year > yearTo){
        console.log("Error, Year From can't be higher than Year To.")
        document.getElementById("yearFromInput").value = yearFrom
        return false
    }
    yearFrom = year
    document.getElementById("yearFromOutput").value = yearFrom
    return true
}

function setYearTo(year) {
    if (year < yearFrom){
        console.log("Error, Year To can't be higher than Year From.")
        document.getElementById("yearToInput").value = yearTo
        return false
    }
    yearTo = year
    document.getElementById("yearToOutput").value = yearTo
    return true
}